from django.shortcuts import render
from django.urls import reverse
from django.db import connection
from django.http import HttpResponseRedirect
from django.contrib import messages

def transaksi(request):
	cursor = connection.cursor()
	cursor.execute('set search_path to bike_sharing')
	response={}
	cursor.execute("SELECT T.no_kartu_anggota,T.date_time,T.jenis,T.nominal from TRANSAKSI as T, ANGGOTA as A, PERSON as P where A.ktp= P.ktp and T.no_kartu_anggota = A.no_kartu;")
	hasil = cursor.fetchall()
	response['transaksi'] = hasil
	print(response)
	return render (request,'transaksi.html',response)
	
def laporan(request):
	cursor = connection.cursor()
	cursor.execute('set search_path to bike_sharing')
	response={}
	cursor.execute("SELECT L.no_kartu_anggota,L.id_laporan,L.datetime_pinjam,M.nama,P.denda, L.status  from person as M, LAPORAN as L,  ANGGOTA as A, PEMINJAMAN as P where M.ktp = A.ktp and A.no_kartu = L.no_kartu_anggota and L.no_kartu_anggota = P.no_kartu_anggota and L.datetime_pinjam= P.datetime_pinjam and L.nomor_sepeda = P.nomor_sepeda ;")
	hasil = cursor.fetchall()
	response['laporan'] = hasil
	print(response)
	return render (request, 'laporan.html', response)

	
def topup(request):
    return render (request, 'topup.html')
	
def topupfunc(request):
	cursor = connection.cursor()
	cursor.execute('set search_path to bike_sharing')
	if(request.method == "POST"):
		saldo = request.POST['topup']    
		cursor.execute("SELECT no_kartu from ANGGOTA as A, PERSON as P where A.ktp = P.ktp")
		kartu=cursor.fetchone()
		kartu=kartu[0]
		cursor.execute("set timezone='asia/jakarta';")
		cursor.execute("INSERT INTO TRANSAKSI(no_kartu_anggota,date_time,jenis,nominal)values('"+str(kartu)+"',now()::timestamp,'topup','"+saldo+"')")
		messages.error(request, "Top Up Berhasil!")
		return HttpResponseRedirect('/transaksi/topup')
	else:
		messages.error(request, "Transaksi Gagal!")
		return HttpResponseRedirect('/transaksi/topup')


		



# Create your views here.
