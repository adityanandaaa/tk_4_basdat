from django.shortcuts import render
from django.db import connection

# Create your views here.
def stasiuncreate(request):
    return render(request, 'stasiun.html')

def stasiunlist(request):
    response={}
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_station,bike_sharing')
    cursor.execute("select st.id_stasiun,st.nama,st.alamat,st.lat,st.long from stasiun st;")

    hasil1 = cursor.fetchall()
    response['stasiun'] = hasil1
   

 
    return render(request, 'liststasiun.html',response)