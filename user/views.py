from django.shortcuts import render
from django.db import connection
from django.urls import reverse
from django.contrib import messages
from random import randint
from django.http import HttpResponseRedirect


# Create your views here.

def login(request):
	if('nama' in request.session.keys()):
		return HttpResponseRedirect('/')
		
	return render(request,'login.html')
	
def logout(request):
	request.session.flush()
	return HttpResponseRedirect('/')


def registerAnggota(request):
	return render(request,'registerAnggota.html')


def registerPetugas(request):
	return render(request,'registerPetugas.html')
