from django.urls import path
from django.conf.urls import url
from . import views



app_name = 'user'

urlpatterns = [
    path('login/', views.login, name='login'),
	path('registerAnggota/', views.registerAnggota, name='registerAnggota'),
	path('registerPetugas/', views.registerPetugas, name='registerPetugas'),
    
]

