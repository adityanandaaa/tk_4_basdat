from django.shortcuts import render
from django.db import connection

# Create your views here.
def penugasancreate(request):
    return render(request, 'penugasan.html')

def penugasanlist(request):
    response={}
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_station,bike_sharing')
    cursor.execute(" select pen.ktp, per.nama, pen.start_datetime, pen.end_datetime, st.nama from penugasan pen,person per,stasiun st,petugas pet where pen.ktp = pet.ktp and pen.id_stasiun = st.id_stasiun and pet.ktp = per.ktp;")
    
    hasil1 = cursor.fetchall()
    response['penugasan'] = hasil1
   

 
    return render(request, 'listpenugasan.html',response)