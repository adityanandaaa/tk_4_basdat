--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.22
-- Dumped by pg_dump version 11.3 (Ubuntu 11.3-0ubuntu0.19.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: bike_sharing; Type: SCHEMA; Schema: -; Owner: db2018046
--

CREATE SCHEMA bike_sharing;


ALTER SCHEMA bike_sharing OWNER TO db2018046;

--
-- Name: add_transaksi_pinjam(); Type: FUNCTION; Schema: bike_sharing; Owner: db2018046
--

CREATE FUNCTION bike_sharing.add_transaksi_pinjam() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE 
date_time TIMESTAMP;
BEGIN
SELECT T.date_time INTO date_time
FROM TRANSAKSI T, PEMINJAMAN P
WHERE P.no_kartu_anggota = T.no_kartu_anggota;
INSERT INTO TRANSAKSI_KHUSUS_PEMINJAMAN VALUES
(NEW.no_kartu_anggota, date_time, NEW.datetime_pinjam, NULL, NULL, NULL);
END;
$$;


ALTER FUNCTION bike_sharing.add_transaksi_pinjam() OWNER TO db2018046;

--
-- Name: biaya_pinjam(); Type: FUNCTION; Schema: bike_sharing; Owner: db2018046
--

CREATE FUNCTION bike_sharing.biaya_pinjam() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN 
IF ((A.saldo - NEW.saldo )= 0) THEN
UPDATE ANGGOTA A SET A.saldo = A.saldo - NEW.biaya
WHERE A.no_kartu = NEW.no_kartu_anggota;
UPDATE ANGGOTA A SET A.poin = A.poin + 1000
WHERE A.no_kartu = NEW.no_kartu_anggota;
ELSE
UPDATE ANGGOTA A SET A.saldo = 0
WHERE A.no_kartu = NEW.no_kartu_anggota;
UPDATE ANGGOTA A SET A.poin = A.poin + 1000
WHERE A.no_kartu = NEW.no_kartu_anggota;
END IF;
END;
$$;


ALTER FUNCTION bike_sharing.biaya_pinjam() OWNER TO db2018046;

--
-- Name: denda(); Type: FUNCTION; Schema: bike_sharing; Owner: db2018046
--

CREATE FUNCTION bike_sharing.denda() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF(TG_OP) = ‘UPDATE’ THEN
IF (EXTRACT (EPOCH FROM(
PEMINJAMAN.datetime_kembali - PEMINJAMAN.datetime_pinjam)) > 36000) THEN
UPDATE PEMINJAMAN SET 
PEMINJAMAN.denda = PEMINJAMAN.denda + ((EXTRACT (EPOCH FROM(
PEMINJAMAN.datetime_kembali - PEMINJAMAN.datetime_pinjam)) / 3600) * 50000)
WHERE NEW.no_kartu_anggota = OLD.no_kartu_anggota;
ELSIF (EXTRACT (EPOCH FROM(
PEMINJAMAN.datetime_kembali - PEMINJAMAN.datetime_pinjam)) > 86400) THEN
UPDATE PEMINJAMAN SET
PEMINJAMAN.denda = PEMINJAMAN.denda + 3000000
WHERE NEW.no_kartu_anggota = OLD.no_kartu_anggota;
END IF;
END IF;
END
$$;


ALTER FUNCTION bike_sharing.denda() OWNER TO db2018046;

--
-- Name: laporan_24jam(); Type: FUNCTION; Schema: bike_sharing; Owner: db2018046
--

CREATE FUNCTION bike_sharing.laporan_24jam() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
new_id VARCHAR (10);
selected_id VARCHAR(10);
BEGIN
IF(TG_OP) = 
'UPDATE' THEN 
SELECT L.id_laporan FROM LAPORAN L
ORDER BY CAST(L.id_laporan AS INTEGER) DESC
LIMIT 1 INTO selected_id;
SELECT CAST((CAST(selected_id AS INTEGER) + 1)
AS VARCHAR) INTO new_id;
IF(EXTRACT (EPOCH FROM(
NEW.datetime_kembali - OLD.datetime_pinjam)) > 86400) THEN
INSERT INTO LAPORAN VALUES (new_id, NEW.no_kartu_anggota, NEW.datetime_pinjam,
NEW.nomor_sepeda, NEW.id_stasiun, 
'Lebih dari 24 jam');END IF; 
RETURN NEW;
END IF;
END
$$;


ALTER FUNCTION bike_sharing.laporan_24jam() OWNER TO db2018046;

--
-- Name: top_up(); Type: FUNCTION; Schema: bike_sharing; Owner: db2018046
--

CREATE FUNCTION bike_sharing.top_up() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
UPDATE ANGGOTA A SET A.saldo = A.saldo + NEW.nominal
WHERE A.no_kartu = NEW.no_kartu_anggota;
END;
$$;


ALTER FUNCTION bike_sharing.top_up() OWNER TO db2018046;

--
-- Name: tukar_voucher(); Type: FUNCTION; Schema: bike_sharing; Owner: db2018046
--

CREATE FUNCTION bike_sharing.tukar_voucher() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN 
IF ((A.poin - NEW.nilai_poin )= 0) THEN
UPDATE ANGGOTA A SET A.poin = A.poin - NEW.nilai_poin
WHERE A.no_kartu = NEW.no_kartu_anggota;
ELSE
UPDATE ANGGOTA A SET A.poin = 0
WHERE A.no_kartu = NEW.no_kartu_anggota;
END IF;
END;
$$;


ALTER FUNCTION bike_sharing.tukar_voucher() OWNER TO db2018046;

--
-- Name: add_transaksi_pinjam(); Type: FUNCTION; Schema: public; Owner: db2018046
--

CREATE FUNCTION public.add_transaksi_pinjam() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE 
date_time TIMESTAMP;
BEGIN
SELECT T.date_time INTO date_time
FROM TRANSAKSI T, PEMINJAMAN P
WHERE P.no_kartu_anggota = T.no_kartu_anggota;
INSERT INTO TRANSAKSI_KHUSUS_PEMINJAMAN VALUES
(NEW.no_kartu_anggota, date_time, NEW.datetime_pinjam, NULL, NULL, NULL);
END;
$$;


ALTER FUNCTION public.add_transaksi_pinjam() OWNER TO db2018046;

--
-- Name: top_up(); Type: FUNCTION; Schema: public; Owner: db2018046
--

CREATE FUNCTION public.top_up() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
UPDATE ANGGOTA A SET A.saldo = A.saldo + NEW.nominal
WHERE A.no_kartu = NEW.no_kartu_anggota;
END;
$$;


ALTER FUNCTION public.top_up() OWNER TO db2018046;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: acara; Type: TABLE; Schema: bike_sharing; Owner: db2018046
--

CREATE TABLE bike_sharing.acara (
    id_acara character varying(10) NOT NULL,
    judul character varying(100) NOT NULL,
    deskripsi text,
    tgl_mulai date NOT NULL,
    tgl_akhir date NOT NULL,
    is_free boolean NOT NULL
);


ALTER TABLE bike_sharing.acara OWNER TO db2018046;

--
-- Name: acara_stasiun; Type: TABLE; Schema: bike_sharing; Owner: db2018046
--

CREATE TABLE bike_sharing.acara_stasiun (
    id_stasiun character varying(10) NOT NULL,
    id_acara character varying(10) NOT NULL
);


ALTER TABLE bike_sharing.acara_stasiun OWNER TO db2018046;

--
-- Name: anggota; Type: TABLE; Schema: bike_sharing; Owner: db2018046
--

CREATE TABLE bike_sharing.anggota (
    no_kartu character varying(10) NOT NULL,
    saldo real,
    points integer,
    ktp character varying(20) NOT NULL
);


ALTER TABLE bike_sharing.anggota OWNER TO db2018046;

--
-- Name: laporan; Type: TABLE; Schema: bike_sharing; Owner: db2018046
--

CREATE TABLE bike_sharing.laporan (
    id_laporan character varying(10) NOT NULL,
    no_kartu_anggota character varying(10) NOT NULL,
    datetime_pinjam timestamp without time zone NOT NULL,
    nomor_sepeda character varying(10) NOT NULL,
    id_stasiun character varying(10) NOT NULL,
    status character varying(20) NOT NULL
);


ALTER TABLE bike_sharing.laporan OWNER TO db2018046;

--
-- Name: peminjaman; Type: TABLE; Schema: bike_sharing; Owner: db2018046
--

CREATE TABLE bike_sharing.peminjaman (
    no_kartu_anggota character varying(10) NOT NULL,
    datetime_pinjam timestamp without time zone NOT NULL,
    nomor_sepeda character varying(10) NOT NULL,
    id_stasiun character varying(10) NOT NULL,
    datetime_kembali timestamp without time zone NOT NULL,
    biaya real NOT NULL,
    denda real NOT NULL
);


ALTER TABLE bike_sharing.peminjaman OWNER TO db2018046;

--
-- Name: penugasan; Type: TABLE; Schema: bike_sharing; Owner: db2018046
--

CREATE TABLE bike_sharing.penugasan (
    ktp character varying(20) NOT NULL,
    start_datetime timestamp without time zone NOT NULL,
    id_stasiun character varying(10) NOT NULL,
    end_datetime timestamp without time zone NOT NULL
);


ALTER TABLE bike_sharing.penugasan OWNER TO db2018046;

--
-- Name: person; Type: TABLE; Schema: bike_sharing; Owner: db2018046
--

CREATE TABLE bike_sharing.person (
    ktp character varying(20) NOT NULL,
    email character varying(50) NOT NULL,
    nama character varying(50) NOT NULL,
    alamat text,
    tgl_lahir date NOT NULL,
    no_telp character varying(20)
);


ALTER TABLE bike_sharing.person OWNER TO db2018046;

--
-- Name: petugas; Type: TABLE; Schema: bike_sharing; Owner: db2018046
--

CREATE TABLE bike_sharing.petugas (
    ktp character varying(20) NOT NULL,
    gaji real NOT NULL
);


ALTER TABLE bike_sharing.petugas OWNER TO db2018046;

--
-- Name: sepeda; Type: TABLE; Schema: bike_sharing; Owner: db2018046
--

CREATE TABLE bike_sharing.sepeda (
    nomor character varying(10) NOT NULL,
    merk character varying(10) NOT NULL,
    jenis character varying(50) NOT NULL,
    status boolean NOT NULL,
    id_stasiun character varying(10) NOT NULL,
    no_kartu_penyumbang character varying(20)
);


ALTER TABLE bike_sharing.sepeda OWNER TO db2018046;

--
-- Name: stasiun; Type: TABLE; Schema: bike_sharing; Owner: db2018046
--

CREATE TABLE bike_sharing.stasiun (
    id_stasiun character varying(10) NOT NULL,
    alamat text NOT NULL,
    lat real,
    long real,
    nama character varying(50) NOT NULL
);


ALTER TABLE bike_sharing.stasiun OWNER TO db2018046;

--
-- Name: transaksi; Type: TABLE; Schema: bike_sharing; Owner: db2018046
--

CREATE TABLE bike_sharing.transaksi (
    no_kartu_anggota character varying(10) NOT NULL,
    date_time timestamp without time zone NOT NULL,
    jenis character varying(20) NOT NULL,
    nominal real NOT NULL
);


ALTER TABLE bike_sharing.transaksi OWNER TO db2018046;

--
-- Name: transaksi_khusus_peminjaman; Type: TABLE; Schema: bike_sharing; Owner: db2018046
--

CREATE TABLE bike_sharing.transaksi_khusus_peminjaman (
    no_kartu_anggota character varying(10) NOT NULL,
    date_time timestamp without time zone NOT NULL,
    no_kartu_peminjam character varying(10),
    datetime_pinjam timestamp without time zone,
    no_sepeda character varying(10),
    id_stasiun character varying(10)
);


ALTER TABLE bike_sharing.transaksi_khusus_peminjaman OWNER TO db2018046;

--
-- Name: voucher; Type: TABLE; Schema: bike_sharing; Owner: db2018046
--

CREATE TABLE bike_sharing.voucher (
    id_voucher character varying(10) NOT NULL,
    nama character varying(255) NOT NULL,
    kategori character varying(255) NOT NULL,
    nilai_poin real NOT NULL,
    deskripsi text,
    no_kartu_anggota character varying(10)
);


ALTER TABLE bike_sharing.voucher OWNER TO db2018046;

--
-- Data for Name: acara; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018046
--

COPY bike_sharing.acara (id_acara, judul, deskripsi, tgl_mulai, tgl_akhir, is_free) FROM stdin;
8519893829	Soup - Campbells, Spinach Crm	Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.	2018-04-11	2018-06-20	t
7228021538	Swordfish Loin Portions	Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.	2018-11-28	2018-04-14	f
7909211805	Spice - Pepper Portions	Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.	2019-02-11	2018-12-01	t
8538119667	Salt - Sea	Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.	2018-09-16	2018-11-01	t
6340003130	Oregano - Fresh	Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.	2018-07-24	2019-01-24	t
5558770103	Sprite, Diet - 355ml	Fusce consequat. Nulla nisl. Nunc nisl.Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.	2018-04-20	2019-02-03	t
4367627189	Plate - Foam, Bread And Butter	Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.	2018-11-18	2018-07-02	f
1078675792	Ice Cream - Vanilla	Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.	2018-12-03	2018-07-14	f
8787015249	Oil - Macadamia	Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.	2018-10-02	2019-02-21	f
2197459634	Salad Dressing	Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.	2018-04-10	2018-07-03	f
8253911181	Pygmy Rose	In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.	2019-01-20	2018-09-08	f
2383950491	Tobias' Saxifrage	In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.Maecenas leo odio, condimentum id, luctus nec, molestie  sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.	2018-06-26	2018-10-14	t
7290191625	Brewer's Erigeron	Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.	2018-10-09	2018-10-14	f
6254195984	Castela	Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.	2018-09-12	2018-11-19	f
9679046397	O'ahu Cyanea	Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.	2018-09-21	2018-10-30	t
2121936769	Dot Lichen	Fusce consequat. Nulla nisl. Nunc nisl.	2018-10-25	2018-10-24	f
7575263888	Norway Sedge	Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.	2018-10-01	2019-02-11	f
4109014808	Grand Fir	Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo.	2019-03-08	2018-11-04	t
7285274739	Smallstalk Necklace Fern	Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.	2018-08-22	2019-03-23	t
704919796	Smallflower Lupine	Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.	2019-01-26	2018-07-06	f
7823938170	White Tackstem	Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.	2019-01-20	2018-06-08	t
4746240930	Plummer's Candyleaf	Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui.	2019-01-20	2019-02-23	t
1053056974	Manycolor Rimmed Lichen	Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.	2018-05-25	2018-07-15	t
3945873436	Beckwith's Violet	In congue. Etiam justo. Etiam pretium iaculis justo.In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.	2018-05-01	2018-07-26	t
238210219	False Breadnut	Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.	2018-09-06	2018-07-08	f
\.


--
-- Data for Name: acara_stasiun; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018046
--

COPY bike_sharing.acara_stasiun (id_stasiun, id_acara) FROM stdin;
6312063456	8519893829
1929769138	7228021538
8636977565	7909211805
2049674987	8538119667
8151178731	6340003130
9463608387	5558770103
1278434561	4367627189
1184421192	1078675792
5495170035	8787015249
1408981964	2197459634
5112382019	8253911181
8121095458	2383950491
9733875382	7290191625
2740942572	6254195984
6356967539	9679046397
3264485332	2121936769
1472497806	7575263888
3381904511	4109014808
9894308870	7285274739
8906292872	704919796
3011382212	7823938170
4924518379	4746240930
893668168	1053056974
7958803737	3945873436
8002613244	238210219
\.


--
-- Data for Name: anggota; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018046
--

COPY bike_sharing.anggota (no_kartu, saldo, points, ktp) FROM stdin;
1	72.9000015	60	108-95-3001
2	10.0600004	66	387-15-8202
3	42.9399986	53	137-11-1941
4	53.5099983	69	824-62-0013
5	38.5	27	286-45-8838
6	20.7600002	99	342-41-8840
7	88.4800034	39	898-39-5188
8	62.0200005	3	761-32-3850
9	53.7700005	53	354-15-3869
10	31.0799999	54	560-66-9151
11	39.0900002	36	168-77-6760
12	83.0999985	80	812-86-1947
13	57.7900009	45	736-50-3164
14	92.0999985	50	726-82-7650
15	59.7400017	17	510-53-3677
16	38.0499992	26	252-61-8543
17	87.9300003	48	810-53-2306
18	46.9500008	70	837-42-6126
19	6.46999979	1	433-66-8689
20	50.6800003	38	330-63-6712
21	64.0800018	58	321-23-8091
22	80.1800003	58	746-39-4140
23	73.3099976	25	476-56-3886
24	65.3099976	56	538-02-3323
25	16.8299999	89	891-66-4376
26	39.7400017	66	311-41-8405
27	11.3299999	15	101-46-9039
28	59.2999992	59	482-65-7191
29	58.8100014	73	490-73-6193
30	75.1200027	78	130-73-2902
31	46.4199982	37	694-53-8531
32	24.75	14	628-83-4780
33	89.2600021	31	602-61-9015
34	11.29	22	665-23-9066
35	99	27	199-51-9264
36	68.5500031	85	161-50-0267
37	83.2200012	6	208-25-3351
38	77.4300003	56	239-60-4110
39	70.3899994	66	284-97-5150
40	78.7900009	44	703-88-5362
41	34.3699989	90	476-58-1142
42	92.8099976	48	434-49-9046
43	40.9000015	30	582-10-3088
44	56.5499992	62	449-94-3092
45	37.4700012	29	322-46-4049
46	18.1399994	9	759-15-0538
47	40.7400017	62	278-86-7760
48	4.32000017	54	492-70-4121
49	84.4300003	56	724-55-1365
50	62.0600014	95	344-52-3459
51	68.1999969	54	323-41-5419
52	66.9400024	40	196-07-9567
53	20.3500004	61	800-65-7378
54	32.3600006	76	514-13-6158
55	22.9099998	19	830-21-0588
56	2.72000003	36	182-12-7961
57	84.3499985	85	179-21-4221
58	4.36999989	23	502-11-7838
59	75.1399994	81	101-12-9342
60	44.8899994	61	487-46-9139
61	14.0799999	16	117-89-2515
62	81.8399963	30	427-28-8063
63	10.5699997	52	417-05-3514
64	79.7300034	37	458-78-6603
65	17.7600002	20	352-61-1846
66	12.3400002	6	545-74-1548
67	39.4900017	3	892-71-9470
68	32.9399986	20	152-93-0898
69	0.600000024	5	350-40-5258
70	93.2600021	20	133-33-8347
71	18.5599995	90	569-94-1514
72	87.8300018	70	276-96-0050
73	48.2400017	23	844-60-9114
74	82.9599991	37	336-61-9516
75	53.1699982	98	184-35-8724
76	28.8099995	32	453-54-9199
77	30.6599998	68	479-06-0310
78	70.4100037	68	723-61-6761
79	40.8699989	29	873-91-9145
80	86.3099976	35	224-62-3634
81	24.1900005	26	432-01-3112
82	62.25	63	574-77-7144
83	80.4400024	50	624-25-1317
84	56.0499992	71	198-02-4091
85	93.4000015	17	106-60-5153
86	16.1000004	45	710-92-4432
87	67.8799973	47	110-39-5149
88	80.0699997	45	863-02-0362
89	91.4599991	19	466-07-4294
90	72.6999969	26	408-80-0656
91	83.4800034	97	436-59-8099
92	57.6599998	79	507-79-5772
93	52.7000008	54	506-21-1908
94	48.8699989	28	733-92-7191
95	42.3300018	19	620-61-8327
96	72.1100006	32	627-33-8641
97	30.6100006	44	333-20-7246
98	20.1499996	25	757-91-0755
99	68.6399994	96	137-41-6721
100	85.7200012	62	285-87-0781
\.


--
-- Data for Name: laporan; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018046
--

COPY bike_sharing.laporan (id_laporan, no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun, status) FROM stdin;
KX23	1	2019-02-07 14:03:00	392	6312063456	Fusce congue.
CU51	3	2019-01-13 13:15:00	265	8636977565	Cum sociis.
VL56	4	2019-01-04 21:37:00	389	2049674987	Nullam porttitor.
UK39	5	2019-01-02 13:29:00	492	8151178731	Integer tincidunt.
YV84	6	2019-02-20 12:18:00	836	9463608387	Aenean fermentum.
FD27	7	2019-01-04 17:03:00	68	1278434561	In eleifend quam
EO08	8	2019-01-21 20:31:00	7	1184421192	Aenean auctor.
LH52	9	2019-01-04 21:57:00	258	5495170035	Fusce consequat.
HR22	10	2019-01-02 13:20:00	208	1408981964	Vivamus tortor.
\.


--
-- Data for Name: peminjaman; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018046
--

COPY bike_sharing.peminjaman (no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun, datetime_kembali, biaya, denda) FROM stdin;
1	2019-02-07 14:03:00	392	6312063456	2019-02-16 23:41:00	135460	681686
2	2019-01-04 03:39:00	801	1929769138	2019-02-08 18:18:00	204594	370506
3	2019-01-13 13:15:00	265	8636977565	2019-02-26 18:13:00	141477	570723
4	2019-01-04 21:37:00	389	2049674987	2019-02-15 12:50:00	205462	208285
5	2019-01-02 13:29:00	492	8151178731	2019-01-16 16:30:00	112203	970916
6	2019-02-20 12:18:00	836	9463608387	2019-02-21 06:08:00	209936	748263
7	2019-01-04 17:03:00	68	1278434561	2019-02-13 02:45:00	206281	514665
8	2019-01-21 20:31:00	7	1184421192	2019-02-17 00:16:00	233000	812181
9	2019-01-04 21:57:00	258	5495170035	2019-01-10 04:00:00	137486	370546
10	2019-01-02 13:20:00	208	1408981964	2019-01-17 11:07:00	169347	879489
11	2019-01-05 09:14:00	724	5112382019	2019-02-20 22:36:00	61563	865220
12	2019-01-20 18:46:00	745	8121095458	2019-02-20 06:52:00	233348	632607
13	2019-01-09 06:33:00	123	9733875382	2019-02-15 15:02:00	238693	694788
14	2019-01-11 00:59:00	452	2740942572	2019-02-08 04:23:00	45238	662253
15	2019-01-12 08:22:00	760	6356967539	2019-02-22 23:55:00	196167	54252
16	2019-01-03 15:35:00	147	3264485332	2019-01-26 01:30:00	87173	189335
17	2019-01-16 04:40:00	307	1472497806	2019-02-13 23:31:00	77368	862497
18	2019-01-05 18:27:00	477	3381904511	2019-02-26 12:10:00	54851	389168
19	2019-01-13 07:09:00	435	9894308870	2019-01-31 04:50:00	95346	808061
20	2019-02-16 03:54:00	947	8906292872	2019-02-20 20:03:00	88173	904371
21	2019-01-09 20:01:00	14	3011382212	2019-02-21 10:45:00	186152	664535
22	2019-02-04 07:32:00	885	1992476795	2019-02-07 23:45:00	100245	917421
23	2019-01-01 17:27:00	300	7058074250	2019-01-15 02:28:00	63036	446227
24	2019-01-03 05:33:00	717	7985455325	2019-02-07 05:35:00	114918	700177
25	2019-02-03 19:25:00	229	1521068163	2019-02-13 14:19:00	228660	150008
26	2019-01-10 09:47:00	71	2835053823	2019-02-15 19:21:00	52159	189259
27	2019-01-03 08:51:00	561	5485695175	2019-02-05 19:20:00	90459	316512
28	2019-01-16 18:39:00	425	5643252122	2019-02-15 05:41:00	181677	121360
29	2019-01-29 12:45:00	549	9666881568	2019-02-24 05:03:00	70669	306360
30	2019-01-29 02:50:00	97	4423323442	2019-02-14 04:59:00	136216	413693
31	2019-02-11 06:59:00	302	5590969344	2019-02-24 12:41:00	47281	203835
32	2019-02-08 15:20:00	780	6054624350	2019-02-15 16:39:00	192648	178441
33	2019-01-12 20:44:00	325	5659196726	2019-01-15 11:58:00	141583	64330
34	2019-01-29 07:05:00	112	3976597021	2019-02-03 08:27:00	176849	622079
35	2019-01-06 18:27:00	4250	2755842610	2019-02-26 17:24:00	136341	540383
36	2019-01-11 16:12:00	567	2037185186	2019-01-14 06:17:00	124372	84397
37	2019-01-30 16:01:00	428	8610262819	2019-02-19 04:14:00	52714	772573
38	2019-01-02 05:54:00	104	9603688799	2019-02-11 09:49:00	15434	176360
39	2019-02-10 17:16:00	69	3837112060	2019-02-23 18:33:00	89481	118792
40	2019-02-03 00:16:00	280	9873285052	2019-02-26 22:41:00	109396	435935
41	2019-01-09 10:49:00	228	1223890438	2019-02-04 10:59:00	181336	205141
42	2019-01-22 02:25:00	27	4379751506	2019-02-26 19:50:00	118245	851100
43	2019-01-26 06:47:00	442	4062913221	2019-02-01 22:07:00	107255	234885
44	2019-01-19 14:00:00	627	4752218682	2019-02-07 12:19:00	201663	683777
45	2019-01-19 01:34:00	60	1270208460	2019-02-06 09:28:00	207205	488992
46	2019-01-11 01:09:00	205	5442841814	2019-02-04 21:52:00	163624	872984
47	2019-02-01 07:18:00	534	8518426980	2019-02-21 19:09:00	198584	378650
48	2019-01-05 11:46:00	822	1775143765	2019-02-05 20:14:00	190836	217900
49	2019-01-17 10:49:00	701	2387465290	2019-02-12 00:56:00	140996	860107
50	2019-01-15 20:54:00	153	4428938386	2019-01-28 15:36:00	23523	20866
51	2019-06-19 20:54:00	850	9861679673	2019-12-24 04:36:00	681686	135460
52	2019-09-30 20:41:00	936	4327303813	2019-01-13 11:23:00	370506	204594
53	2019-05-16 14:17:00	973	7471830081	2019-08-19 08:19:00	570723	141477
54	2019-02-06 07:30:00	802	6302348535	2019-08-25 10:45:00	208285	205462
55	2019-12-19 12:36:00	253	1928294454	2019-09-08 03:54:00	970916	112203
56	2019-05-26 14:44:00	746	7920193925	2019-07-04 10:09:00	748263	209936
57	2019-02-17 23:15:00	533	8163912810	2019-02-04 04:10:00	514665	206281
58	2019-09-27 21:03:00	264	8064881764	2019-03-12 06:56:00	812181	233000
59	2019-09-29 23:15:00	960	5616283098	2019-01-11 12:51:00	370546	137486
60	2019-10-08 13:41:00	222	8564122545	2019-04-03 11:00:00	879489	169347
61	2019-01-02 15:29:00	131	7869118997	2019-06-12 12:58:00	865220	61563
62	2019-06-04 05:34:00	161	7417583551	2019-12-22 11:23:00	632607	233348
63	2019-12-30 16:22:00	515	9397281364	2019-02-13 12:58:00	694788	238693
64	2019-05-01 23:56:00	479	2995527281	2019-10-04 05:04:00	662253	45238
65	2019-05-04 00:46:00	223	7800955310	2019-03-09 05:18:00	54252	196167
66	2019-11-21 00:35:00	620	2728316655	2019-07-30 02:39:00	189335	87173
67	2019-03-22 10:49:00	710	4147615340	2019-01-17 10:20:00	862497	77368
68	2019-05-13 11:57:00	762	4755876679	2019-12-07 08:21:00	389168	54851
69	2019-11-22 02:02:00	113	6394905279	2019-10-15 11:07:00	808061	95346
70	2019-04-20 04:18:00	140	4813444075	2019-05-29 11:21:00	904371	88173
71	2019-01-21 23:17:00	305	4105629076	2019-10-14 07:02:00	664535	186152
72	2019-08-30 22:41:00	725	9262122055	2019-03-25 11:14:00	917421	100245
73	2019-04-01 12:10:00	882	4638515017	2019-06-19 10:31:00	446227	63036
74	2019-12-18 15:09:00	847	6794397584	2019-06-16 11:33:00	700177	114918
75	2019-07-23 01:58:00	648	6252197953	2019-01-14 05:59:00	150008	228660
76	2019-11-30 12:10:00	600	6626329256	2019-04-02 12:27:00	189259	52159
77	2019-02-18 02:05:00	346	9229997432	2019-10-22 12:18:00	316512	90459
78	2019-03-11 21:48:00	261	4072279458	2019-08-03 04:30:00	121360	181677
79	2019-06-22 16:26:00	834	7087202701	2019-05-19 01:48:00	306360	70669
80	2019-02-20 08:25:00	981	9660335346	2019-05-22 02:27:00	413693	136216
81	2019-04-03 13:42:00	277	6076291031	2019-08-25 09:39:00	203835	47281
82	2019-08-13 10:15:00	813	8322948117	2019-09-28 01:19:00	178441	192648
83	2019-09-10 20:11:00	828	1133043208	2019-08-16 01:20:00	64330	141583
84	2019-03-07 05:24:00	447	1356639989	2019-01-23 07:40:00	622079	176849
85	2019-02-14 07:13:00	935	9569758644	2019-01-18 02:53:00	540383	136341
86	2019-10-07 18:52:00	154	4454459520	2019-05-21 09:27:00	84397	124372
87	2019-04-20 19:40:00	985	7013257514	2019-01-05 07:22:00	772573	52714
88	2019-06-04 00:34:00	240	8241734299	2019-06-15 08:40:00	176360	15434
89	2019-01-14 00:49:00	853	9814019320	2019-01-31 01:37:00	118792	89481
90	2019-04-27 09:45:00	737	3038047517	2019-03-19 10:29:00	435935	109396
91	2019-05-30 21:18:00	372	3460252043	2019-03-01 12:08:00	205141	181336
92	2019-05-30 23:37:00	619	9036144918	2019-09-21 02:14:00	851100	118245
93	2019-01-23 11:46:00	218	8172941703	2019-05-16 10:56:00	234885	107255
94	2019-02-05 00:56:00	437	3218561633	2019-03-14 04:29:00	683777	201663
95	2019-02-02 10:34:00	275	3967296473	2019-06-02 01:38:00	488992	207205
96	2019-01-28 17:38:00	605	3058754837	2019-03-14 02:40:00	872984	163624
97	2019-10-02 04:44:00	638	5781975408	2019-12-15 07:24:00	378650	198584
98	2019-06-08 16:38:00	831	8052255775	2019-12-24 04:44:00	217900	190836
99	2019-04-24 11:00:00	658	1245924074	2019-08-06 07:11:00	860107	140996
100	2019-09-07 07:24:00	750	9310711057	2019-09-23 06:27:00	20866	23523
\.


--
-- Data for Name: penugasan; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018046
--

COPY bike_sharing.penugasan (ktp, start_datetime, id_stasiun, end_datetime) FROM stdin;
387-15-8202	2018-11-10 01:55:02	6312063456	2018-09-04 21:22:31
137-11-1941	2018-08-13 22:10:33	1929769138	2018-09-27 04:21:32
108-95-3001	2019-04-03 08:33:32	8636977565	2019-04-09 23:39:07
824-62-0013	2018-05-12 05:37:20	2049674987	2019-02-14 19:51:02
342-41-8840	2018-08-19 14:21:20	8151178731	2018-11-06 09:00:59
761-32-3850	2018-04-22 13:56:15	9894308870	2018-12-25 09:52:21
354-15-3869	2018-12-15 10:56:31	2740942572	2018-05-02 01:19:25
560-66-9151	2019-01-05 16:59:14	1184421192	2019-03-18 11:59:26
168-77-6760	2018-12-18 17:59:32	9733875382	2018-06-01 11:22:21
286-45-8838	2018-10-01 19:59:23	1408981964	2018-11-24 14:34:58
812-86-1947	2018-09-11 22:00:22	5495170035	2018-07-21 20:26:29
726-82-7650	2019-02-26 07:19:15	1408981964	2018-11-16 06:38:29
510-53-3677	2019-01-29 07:49:29	9733875382	2018-12-26 16:21:57
252-61-8543	2019-03-07 02:24:02	2740942572	2018-10-29 00:14:17
321-23-8091	2018-12-05 03:27:51	6356967539	2018-10-12 11:41:23
130-73-2902	2019-04-01 23:48:26	3264485332	2018-05-07 10:33:04
810-53-2306	2018-05-01 18:48:28	1472497806	2018-12-26 03:35:23
837-42-6126	2019-01-14 00:14:35	3381904511	2018-12-12 00:05:35
898-39-5188	2018-11-04 16:07:14	9894308870	2019-03-23 05:41:02
433-66-8689	2018-09-12 18:54:57	8906292872	2019-03-23 00:42:16
330-63-6712	2018-07-19 00:12:34	6356967539	2018-07-18 10:35:33
736-50-3164	2018-11-03 03:02:43	9463608387	2018-12-10 14:46:04
746-39-4140	2018-10-19 00:50:49	9894308870	2018-07-29 20:06:35
476-56-3886	2019-02-23 12:16:53	8121095458	2018-05-02 05:22:08
891-66-4376	2018-05-28 04:59:09	8906292872	2018-09-17 01:36:17
311-41-8405	2019-03-24 23:34:32	2740942572	2018-05-11 01:50:25
101-46-9039	2018-06-17 18:51:19	8906292872	2018-04-21 16:41:23
482-65-7191	2018-08-28 19:43:23	9894308870	2019-01-02 12:51:02
538-02-3323	2019-01-10 08:37:59	5112382019	2018-06-04 06:47:37
490-73-6193	2019-04-05 13:52:43	8906292872	2018-04-22 00:20:59
\.


--
-- Data for Name: person; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018046
--

COPY bike_sharing.person (ktp, email, nama, alamat, tgl_lahir, no_telp) FROM stdin;
108-95-3001	lcrothers0@booking.com	Lisette	3439 Northview Avenue	2019-01-29	16684370
387-15-8202	aquilter1@bloomberg.com	Alikee	60749 Bultman Terrace	2018-04-15	5542744249
137-11-1941	cbozward2@dell.com	Chrissy	39055 Elmside Junction	2019-03-04	9013036678
824-62-0013	afearfull3@hao123.com	Annice	2 Monica Point	2018-06-10	3481765436
286-45-8838	sleveritt4@trellian.com	Sherri	3171 Old Shore Pass	2018-09-29	3305996730
342-41-8840	prhodus5@nationalgeographic.com	Paddie	39 Fieldstone Terrace	2018-06-20	933846029
898-39-5188	clamlin6@bigcartel.com	Carmen	9431 Forest Dale Park	2018-04-20	4303714992
761-32-3850	cranger7@fotki.com	Carmelita	1 Schlimgen Lane	2018-06-05	2707229962
354-15-3869	chackney8@wix.com	Carlita	43342 Utah Court	2018-05-25	4606551267
560-66-9151	egott9@toplist.cz	Elaine	02533 Mallard Court	2019-02-17	6602201581
168-77-6760	lfidgina@dmoz.org	Lucho	8 Clove Hill	2019-03-14	8837406126
812-86-1947	fgullbergb@jalbum.net	Frazer	30941 Dayton Road	2018-09-11	8087903951
736-50-3164	ccordelettec@github.io	Carma	219 Golf Center	2018-09-22	2746063174
726-82-7650	qgladdisd@paypal.com	Quintilla	1180 Clarendon Terrace	2018-07-29	4876208832
510-53-3677	xpetoe@army.mil	Ximenez	05276 Red Cloud Plaza	2019-02-12	855133880
252-61-8543	gheffernonf@si.edu	Guenna	21 Pennsylvania Avenue	2019-02-12	4567983742
810-53-2306	bbillettg@eventbrite.com	Barnabas	9987 Crownhardt Hill	2018-07-31	4780176700
837-42-6126	fpeytonh@dyndns.org	Fernando	0 Bartillon Crossing	2018-06-22	9973330609
433-66-8689	vscimonii@vkontakte.ru	Vasily	97857 Steensland Court	2018-12-18	7273421324
330-63-6712	tbuggyj@oaic.gov.au	Tawsha	5 Fairview Plaza	2019-01-01	9658878733
321-23-8091	hsinnockk@mtv.com	Henrie	9 Mcguire Parkway	2018-08-31	963759248
746-39-4140	phasselll@google.es	Pernell	0884 Sundown Plaza	2018-11-17	2969980118
476-56-3886	dblarem@networksolutions.com	Dare	10412 Kensington Drive	2019-01-15	1942596766
538-02-3323	theinzn@hexun.com	Thorn	5 Walton Junction	2018-09-21	8147092296
891-66-4376	agannicliffto@howstuffworks.com	Anjanette	7868 Thierer Court	2019-01-26	7790739549
311-41-8405	kshoweringp@sfgate.com	Kleon	36676 American Ash Way	2019-02-04	2269533879
101-46-9039	aahrendq@seattletimes.com	Arri	298 Calypso Street	2018-10-20	4632728233
482-65-7191	rletixierr@alibaba.com	Riley	3852 Morningstar Avenue	2018-07-13	1353750396
490-73-6193	ameindls@vkontakte.ru	Adelind	85 Amoth Plaza	2018-06-27	3018162072
130-73-2902	astartint@purevolume.com	Archie	4564 Shopko Trail	2018-04-30	4596034737
694-53-8531	bgiraudyu@webeden.co.uk	Barbara	845 Fuller Trail	2019-01-31	7619924728
628-83-4780	hhaswallv@github.io	Helene	666 Prairie Rose Park	2018-07-23	3203694492
602-61-9015	slodderw@xing.com	Steffie	4171 Logan Trail	2018-12-09	3451923505
665-23-9066	mdentithx@bloomberg.com	Marty	5 Monument Avenue	2019-03-16	8030110634
199-51-9264	tsabeny@answers.com	Tailor	2 Doe Crossing Avenue	2019-02-27	8617481986
161-50-0267	chonywillz@flavors.me	Cherlyn	03 Annamark Trail	2018-09-24	4145724534
208-25-3351	eharwood10@ucoz.ru	Elberta	57 Village Green Junction	2018-12-09	2818677947
239-60-4110	sstorrock11@yahoo.co.jp	Sibylla	1 Kennedy Way	2018-10-29	2867004381
284-97-5150	jlethebridge12@upenn.edu	Jocelin	42761 Buena Vista Alley	2018-12-03	7858282379
703-88-5362	gminnis13@flavors.me	Gilligan	203 Luster Court	2019-04-02	4804571353
476-58-1142	mgalvan14@nature.com	Madel	6 Kedzie Drive	2018-07-23	3646004681
434-49-9046	wboughton15@wunderground.com	Wye	46 Shasta Hill	2018-06-18	6333776920
582-10-3088	unaisey16@china.com.cn	Umberto	4 Fremont Drive	2019-02-10	100508030
449-94-3092	sfaichnie17@jalbum.net	See	62 Ridgeview Pass	2018-07-05	6330638411
322-46-4049	thoworth18@cnbc.com	Teresita	38 Merry Court	2019-03-10	7774816747
759-15-0538	vmcmichan19@macromedia.com	Vannie	41 8th Trail	2019-02-21	5976048471
278-86-7760	dagus1a@storify.com	Drucie	775 La Follette Junction	2018-06-04	3411283270
492-70-4121	ehorsey1b@cbslocal.com	Emily	0708 Esch Junction	2019-02-01	385067852
724-55-1365	smccadden1c@cloudflare.com	Sioux	080 Straubel Road	2018-08-28	5041079676
344-52-3459	spentlow1d@prnewswire.com	Salaidh	083 Green Ridge Hill	2018-11-18	4213988597
323-41-5419	lflook1e@cyberchimps.com	Luci	90 Vahlen Trail	2018-06-19	8976760654
196-07-9567	fshrawley1f@statcounter.com	Fred	1 Huxley Alley	2018-09-24	2024006965
800-65-7378	jligertwood1g@fotki.com	Jaynell	57 Trailsway Parkway	2018-07-23	821174592
514-13-6158	sswindin1h@yahoo.com	Sofia	5 Washington Trail	2019-01-22	5370317399
830-21-0588	jsalmoni1i@reuters.com	Jillana	801 Iowa Circle	2019-03-24	718205952
182-12-7961	rchurchouse1j@skype.com	Rina	510 Main Parkway	2018-10-08	6767331457
179-21-4221	ccoupman1k@slate.com	Chaunce	02 Ohio Pass	2018-10-23	4058020342
502-11-7838	hdixie1l@state.gov	Homer	55969 Dakota Alley	2019-01-28	8832198940
101-12-9342	gbolus1m@weibo.com	Gage	6 Shopko Park	2018-06-23	2086736827
487-46-9139	slemar1n@flickr.com	Shelba	8 Forest Dale Trail	2019-03-16	4370676096
117-89-2515	aryce1o@hubpages.com	Aeriela	46 Londonderry Center	2018-11-27	6144903372
427-28-8063	lmcguigan1p@patch.com	Leontyne	31784 Arapahoe Lane	2018-08-22	8605932562
417-05-3514	kfidian1q@google.com.hk	Kiel	0 Sunfield Drive	2018-07-06	7557367448
458-78-6603	tkilban1r@abc.net.au	Tootsie	6 Carpenter Road	2018-11-22	6142348460
352-61-1846	cbadwick1s@google.nl	Chryste	5 Ruskin Way	2018-08-18	2698479183
545-74-1548	eferrant1t@columbia.edu	Elvin	319 Waxwing Center	2018-12-08	9450719667
892-71-9470	dtolan1u@sina.com.cn	Dulcie	39 Bayside Drive	2018-07-07	75433729
152-93-0898	aidill1v@hp.com	Abram	04 Clove Street	2018-07-23	3313485022
350-40-5258	ljoliffe1w@sakura.ne.jp	Lilllie	60 Marquette Hill	2018-06-19	9879875583
133-33-8347	kscadding1x@so-net.ne.jp	Konstantine	63 Pawling Point	2018-06-27	6193237038
569-94-1514	bbradnock1y@businessinsider.com	Bradly	20 Morrow Parkway	2019-04-04	5713119378
276-96-0050	flidierth1z@auda.org.au	Ford	1357 Montana Street	2018-04-13	9534032905
844-60-9114	bbailles20@amazon.com	Barri	403 Commercial Alley	2019-03-11	8672377036
336-61-9516	lscoggins21@amazon.com	Lowe	92034 Cardinal Drive	2018-04-12	6852400638
184-35-8724	jcanfield22@baidu.com	Janek	5 Morning Alley	2018-12-04	4823895576
453-54-9199	blambrecht23@psu.edu	Barny	42 Coleman Trail	2018-06-29	8291909482
479-06-0310	fdearsley24@w3.org	Farlay	2 Hagan Plaza	2018-05-26	4644810673
723-61-6761	kogan25@studiopress.com	Kent	37 3rd Way	2018-07-31	7457033955
873-91-9145	hhargerie26@quantcast.com	Hilda	5798 Armistice Center	2018-04-22	4676012586
224-62-3634	jjikylls27@cisco.com	Josefina	173 Saint Paul Street	2018-09-22	2056504871
432-01-3112	nroelvink28@bbc.co.uk	Nadia	7 Dryden Road	2018-05-12	9967797835
574-77-7144	sbroke29@livejournal.com	Shaun	3151 Pierstorff Center	2018-07-26	5504880122
624-25-1317	cwheaton2a@upenn.edu	Clayton	5 Messerschmidt Pass	2018-11-30	3885818566
198-02-4091	awaulker2b@tumblr.com	Annette	34191 Buhler Alley	2019-02-04	5551569888
106-60-5153	btousy2c@usatoday.com	Bone	5938 Sheridan Center	2018-09-18	2844595766
710-92-4432	evarley2d@earthlink.net	Em	90 Burrows Crossing	2018-04-15	363942963
110-39-5149	adepaoli2e@miibeian.gov.cn	Alice	4692 Kensington Crossing	2018-11-26	3596390133
863-02-0362	wallright2f@aboutads.info	Wilt	3 Gerald Court	2018-06-25	4346391885
466-07-4294	avasilik2g@plala.or.jp	Arden	21 Cherokee Avenue	2018-06-05	6988880143
408-80-0656	icrawcour2h@statcounter.com	Irving	9463 Schmedeman Pass	2018-11-18	12890359
436-59-8099	tcaen2i@naver.com	Tate	490 Tennyson Crossing	2018-04-23	3105276882
507-79-5772	jwortman2j@icio.us	Jarrad	16370 Grover Avenue	2018-05-15	6110221953
506-21-1908	eenric2k@samsung.com	Ewart	1305 Rigney Point	2018-06-18	9454379291
733-92-7191	ccoldwell2l@google.cn	Crin	7747 Ridge Oak Junction	2018-06-08	406988412
620-61-8327	hkinneally2m@wufoo.com	Hildagarde	17 Steensland Terrace	2019-01-16	2833903855
627-33-8641	aarthey2n@hugedomains.com	Albert	347 Sugar Plaza	2019-03-25	7714358130
333-20-7246	wreolfo2o@gov.uk	Woodman	33299 Holy Cross Plaza	2018-09-12	5125256854
757-91-0755	aboyfield2p@nytimes.com	Ashlee	668 Brown Way	2018-05-04	9124931411
137-41-6721	ecasel2q@domainmarket.com	Emile	31834 Burning Wood Hill	2018-11-28	1442809590
285-87-0781	hjermy2r@businesswire.com	Hardy	22327 Schmedeman Circle	2018-08-03	8339994026
\.


--
-- Data for Name: petugas; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018046
--

COPY bike_sharing.petugas (ktp, gaji) FROM stdin;
387-15-8202	261.26001
137-11-1941	156.020004
108-95-3001	148.25
824-62-0013	151.580002
342-41-8840	272.320007
761-32-3850	200.339996
354-15-3869	276.230011
560-66-9151	210.460007
168-77-6760	182.830002
286-45-8838	199.789993
812-86-1947	244.630005
726-82-7650	163.520004
510-53-3677	148.460007
252-61-8543	236.910004
321-23-8091	250.020004
130-73-2902	269.350006
810-53-2306	215.190002
837-42-6126	143.649994
898-39-5188	288.25
433-66-8689	190.240005
330-63-6712	258.51001
736-50-3164	252.610001
746-39-4140	140.789993
476-56-3886	292.98999
891-66-4376	199.490005
311-41-8405	208
101-46-9039	129.589996
482-65-7191	271.790009
538-02-3323	261.790009
490-73-6193	270.019989
\.


--
-- Data for Name: sepeda; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018046
--

COPY bike_sharing.sepeda (nomor, merk, jenis, status, id_stasiun, no_kartu_penyumbang) FROM stdin;
850	Toyota	K20L6	f	9861679673	51
936	Volvo	R49Z9	f	4327303813	52
973	Oldsmobile	I08U8	f	7471830081	53
802	Mercury	U05U2	f	6302348535	54
253	Pontiac	X52M3	f	1928294454	55
746	Maserati	H63G1	f	7920193925	56
533	Chevrolet	F36H0	t	8163912810	57
264	Chevrolet	Y93X2	t	8064881764	58
960	Mercedes	O32G9	t	5616283098	59
222	Bentley	U19L6	f	8564122545	60
131	Mercury	R65M0	t	7869118997	61
161	Ford	C02G8	t	7417583551	62
515	Jaguar	K46A4	f	9397281364	63
479	Eagle	I74M4	f	2995527281	64
223	Mercedes	B03X2	f	7800955310	65
620	Chevrolet	R22L2	f	2728316655	66
710	Volvo	F95H2	t	4147615340	67
762	Ford	P98Y7	t	4755876679	68
113	Buick	R59R6	f	6394905279	69
140	Subaru	E53W4	t	4813444075	70
305	Land Rover	G81I9	t	4105629076	71
725	Chevrolet	C66Y4	f	9262122055	72
882	Chevrolet	L20Q6	f	4638515017	73
847	Lambo	E47M5	t	6794397584	74
648	Ford	J33S0	t	6252197953	75
600	Mazda	X59C1	f	6626329256	76
346	Toyota	Q67M7	f	9229997432	77
261	Plymouth	P51X2	t	4072279458	78
834	Toyota	Q11O0	t	7087202701	79
981	Toyota	P56T4	f	9660335346	80
277	Isuzu	L20J0	t	6076291031	81
813	Dodge	R20T4	t	8322948117	82
828	Dodge	T44G7	t	1133043208	83
447	Pontiac	U22L0	t	1356639989	84
935	Volkswagen	T07N2	f	9569758644	85
154	Chevrolet	M09L0	t	4454459520	86
985	Mazda	R73K6	f	7013257514	87
240	Chevrolet	Z89Y1	t	8241734299	88
853	Toyota	O93M9	t	9814019320	89
737	Lincoln	V45K6	t	3038047517	90
372	Toyota	W16G4	t	3460252043	91
619	Mitsubishi	X79S5	t	9036144918	92
218	Suzuki	W74M3	t	8172941703	93
437	Hyundai	N59M4	f	3218561633	94
275	Dodge	C09T3	f	3967296473	95
605	Pontiac	X88H8	t	3058754837	96
638	Honda	T57I7	f	5781975408	97
831	Chevrolet	Z12W8	f	8052255775	98
658	Kia	K54H1	f	1245924074	99
750	Mercedes	X16H5	t	9310711057	100
392	Dodge	T80H3	t	6356967539	69
801	Suzuki	Q82L6	t	1929769138	9
265	Dodge	V94X1	f	8151178731	3
389	Pontiac	R66L0	f	2049674987	\N
492	Ford	C99R6	f	8151178731	32
836	Audi	V30X2	f	6356967539	6
68	Chevrolet	P52A5	f	8636977565	\N
7	MINI	G49M8	f	1184421192	8
258	Audi	N03J4	t	9894308870	55
208	Audi	F32C4	t	3264485332	10
724	Dodge	A77L6	t	5112382019	\N
745	Infiniti	B78D5	f	3264485332	\N
123	Buick	Z58D9	t	9733875382	13
452	Hyundai	W75J1	f	2740942572	\N
760	Hyundai	E03Z2	t	6356967539	\N
147	Volvo	Y41I1	f	3264485332	16
307	Honda	K77D3	t	1472497806	\N
477	Mercedes	I71V7	t	3381904511	18
435	Infiniti	B49K7	f	9894308870	2
947	Ford	A63Y4	t	8906292872	19
14	Ford	B63L9	f	2740942572	1
885	Dodge	L87J7	t	8151178731	22
300	Oldsmobile	A82B1	t	9463608387	20
717	Toyota	N82J0	t	8121095458	3
229	Chevrolet	M19Q6	f	1408981964	25
71	Volkswagen	T87S0	t	2740942572	24
561	Acura	K71K9	t	3381904511	\N
425	Volkswagen	F51R0	f	5495170035	28
549	Chevrolet	U00G5	f	6312063456	29
97	Audi	Q83R9	f	5495170035	4
302	Nissan	O69E6	f	9733875382	23
780	Honda	J02J7	t	9463608387	40
325	Volkswagen	D94G6	t	2049674987	33
112	Chevrolet	V16I8	f	8151178731	\N
4250	Acura	I70L9	f	1408981964	\N
567	Volkswagen	B11D1	t	9733875382	\N
428	Mitsubishi	A53O9	f	9894308870	37
104	Ford	S19P9	f	3381904511	\N
69	Acura	W52L3	t	5495170035	39
280	Mercedes	B93X8	t	3381904511	52
228	Mitsubishi	A46W1	t	9733875382	\N
27	GMC	J90J1	f	9463608387	70
442	Chevrolet	G60F7	f	5495170035	\N
627	Acura	Y05L7	t	1408981964	44
60	GMC	H52C5	f	2049674987	62
205	BMW	H47U3	t	8151178731	\N
534	Chevrolet	L85L5	t	6356967539	47
822	Lexus	R07F4	t	2049674987	50
701	BMW	F99Y4	f	8151178731	49
153	Chevrolet	I18X8	t	3381904511	51
\.


--
-- Data for Name: stasiun; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018046
--

COPY bike_sharing.stasiun (id_stasiun, alamat, lat, long, nama) FROM stdin;
6312063456	480 Vermont Park	52.5798378	19.6295357	Field Lovegrass
1929769138	0 Grasskamp Avenue	35.7773476	115.07415	Scarlet Oak
8636977565	86 Westend Court	10.7330523	14.6004915	Pickering's Reedgrass
2049674987	927 2nd Park	55.6607704	52.3473015	Dwarf Checkerbloom
8151178731	698 Gale Way	6.10906506	102.133621	Torrey's Four-nerve Daisy
9463608387	0951 Coolidge Court	52.2454071	16.8470554	Alkali Goldenbush
1278434561	6 Brown Terrace	15.850709	120.39476	Giant Granadilla
1184421192	57546 7th Junction	-19.7999992	32.8666687	Cockspur
5495170035	906 Cordelia Alley	12.6679163	123.988167	Indian Cucumber
1408981964	50 Prentice Center	-23.5795135	-47.9985504	Dot Lichen
5112382019	08675 Porter Way	26.8118668	55.8913193	Haitian Catalpa
8121095458	8 Buhler Court	38.0780945	139.998627	Rimmed Lichen
9733875382	6 Canary Center	-0.788533509	100.654984	Stalked Maidenhair
2740942572	5 Eagle Crest Street	57.4249802	34.9683075	Chihuahuan Sedge
6356967539	545 Towne Pass	33.5200005	-86.8000031	Orange Lichen
3264485332	01164 Garrison Avenue	50.7022247	3.1685214	Western Dwarf Mistletoe
1472497806	8815 Esker Junction	-41.4332199	147.144089	Wild Leadwort
3381904511	6812 Laurel Road	48.3583832	25.3449268	Ma'ohi'ohi
9894308870	08301 Mcbride Way	-0.499328107	37.2784843	Cahaba Prairie Clover
8906292872	5284 Bobwhite Point	44.9009399	129.791946	Golden Trumpet
3011382212	12 Talmadge Plaza	33.1837959	-97.1305542	Ara chloroptera
4924518379	04 Fulton Hill	59.6845932	16.6212711	Tachybaptus ruficollis
893668168	57830 Loomis Trail	-10.1670952	123.578751	Haliaeetus leucocephalus
7958803737	7052 Dakota Alley	-8.29818916	31.5227261	Bison bison
8002613244	4475 Huxley Junction	36.8079071	139.920029	Casmerodius albus
1992476795	72 Corben Terrace	66.2143631	66.6121597	Lilac Chastetree
7058074250	6 Loeprich Street	50.2602272	33.4779739	Datura
7985455325	79 Dawn Plaza	68.4282761	83.3592606	Velvetleaf
1521068163	8 Mendota Point	68.5130005	43.1869507	Field Sagewort
2835053823	6 East Parkway	51.7825928	12.1096087	Heather
5485695175	4990 Mosinee Road	50.1695442	62.5352211	Rimularia Lichen
5643252122	11149 East Pass	37.5615158	50.1292381	Water Mudwort
9666881568	231 Bay Pass	89.8989029	81.5326233	Austrian Draba
4423323442	17 Oneill Circle	18.4371719	55.1842918	Bristly Bedstraw
5590969344	54 Macpherson Street	82.4991608	53.7627869	White Logwood
6054624350	37082 Tony Street	28.7519341	42.3095551	Fuscidea Lichen
5659196726	24 Sunfield Plaza	55.5324211	53.1518288	Shaggy Fleabane
3976597021	68 Tony Point	21.6263714	76.5189056	Little Barley
2755842610	6525 Marquette Terrace	37.3546333	64.9239197	Garden Sorrel
2037185186	119 Browning Park	44.6668777	36.5511169	Pinewoods Horkelia
8610262819	8 Ruskin Crossing	40.4515762	36.9266281	Strigose Bird's-foot Trefoil
9603688799	910 American Circle	48.5035439	82.8682404	Wig Knapweed
3837112060	30809 Porter Parkway	15.9562244	20.6641617	White Alder
9873285052	2 Homewood Park	25.8090572	61.994873	Jungle Rice
1223890438	351 Mosinee Crossing	42.5946045	84.8606415	Wild Bushbean
4379751506	17773 Lillian Park	94.948555	37.7144165	Yucca
4062913221	43 Scofield Drive	11.2099066	96.8752899	Kaulfuss' Spleenwort
4752218682	33226 Rowland Point	31.7840443	17.3885975	Juniper Sedge
1270208460	9224 Pond Avenue	26.5020599	57.5419617	Chaff Flower
5442841814	381 Lukken Center	47.2648621	74.3439026	Letrouitia Lichen
8518426980	57429 Hayes Hill	67.9164276	82.3595734	False Prairie-clover
1775143765	661 Fairview Point	38.2304535	66.9310226	Grassland Nehe
2387465290	1 Monica Avenue	64.1174622	88.0333252	Lollipop-plant
4428938386	7 Forest Dale Trail	60.0119438	89.872879	Concord Hawthorn
9861679673	2 Quincy Avenue	32.1397858	59.3172035	Cusick's Sedge
4327303813	67 Bayside Place	37.1306534	21.7499866	Sonoran Milkvine
7471830081	9 Manitowish Street	32.756237	24.3440056	Rusty Staggerbush
6302348535	96958 Briar Crest Pass	92.5771103	41.7568588	Muehlenberg's Astomum Moss
1928294454	117 Johnson Court	63.2978745	59.4381256	Disjuct Melanelia Lichen
7920193925	6 Bobwhite Alley	42.7695084	31.480072	Lecidea Lichen
8163912810	97774 Brentwood Park	59.1255417	38.9085236	Redflower False Yucca
8064881764	3385 Randy Lane	14.4962997	44.5236969	California Checkerbloom
5616283098	36 Corben Pass	46.4430542	59.4045143	Willowherb
8564122545	023 Miller Court	84.8041153	12.8640022	Southwestern False Cloak Fern
7869118997	6271 Village Green Alley	33.5671577	69.4550095	Prairie Flax
7417583551	757 Fallview Trail	82.2697754	50.4227829	Prairie Phacelia
9397281364	33729 Reindahl Parkway	79.8848648	23.4107857	Mexican Primrose-willow
2995527281	516 Bowman Street	47.4093361	91.8679733	Spring Vetch
7800955310	21248 Almo Crossing	74.8453522	78.9965591	Red Bush Monkeyflower
2728316655	284 Judy Road	82.7816925	84.5298386	Orange Lichen
4147615340	824 Clyde Gallagher Plaza	61.3007011	84.6059494	Twobristle Rockdaisy
4755876679	3 Fairview Avenue	23.6585827	31.4209518	Lamp Rush
6394905279	62 Jenifer Point	10.2082863	74.8889618	Highlands Scrub St. Johnswort
4813444075	70346 Columbus Park	48.5665588	76.584816	Lancewood
4105629076	6 Leroy Center	16.5615444	33.5696945	Thicksepal Cryptantha
9262122055	529 Lakeland Point	84.8476028	33.5183792	Florida Skullcap
4638515017	97026 Londonderry Terrace	31.9114208	88.983551	Bingen Lupine
6794397584	1250 Michigan Center	40.6004944	41.4489174	Fragile Leaf Dicranum Moss
6252197953	69044 Corscot Lane	86.1562576	73.0528412	Palo De Hueso
6626329256	462 Crescent Oaks Trail	18.1085529	72.1122665	Scarlet Oak
9229997432	341 Shoshone Way	29.5410423	50.1839981	Scott Valley Phacelia
4072279458	77534 Ridgeway Hill	55.7894897	27.1469688	Pellitory
7087202701	4 Reindahl Trail	88.8030319	94.7497253	Bentawn Flatsedge
9660335346	4 Cordelia Road	68.892334	69.557869	Santa Cruz Clover
6076291031	67 Duke Parkway	98.0366592	60.617485	Dotted Polypody
8322948117	82 2nd Court	49.0682716	54.3929443	Coastal Searocket
1133043208	4812 Bay Pass	23.9518795	53.5846519	Wondering Cowpea
1356639989	4 Florence Drive	61.419136	68.6989975	Muhlenberg's Funaria Moss
9569758644	05 Maple Crossing	14.0247831	62.3182602	Birdcage Evening Primrose
4454459520	22 Dorton Junction	60.5180206	54.050293	Gundlachia
7013257514	09 Lawn Way	35.2124748	90.289505	Hairy Fogfruit
8241734299	27 Gale Pass	51.7900887	11.2286139	Ives' Phacelia
9814019320	0476 Spohn Court	86.336235	96.8216629	Monterey Larkspur
3038047517	87934 Stone Corner Point	66.6283493	17.4023724	Orcutt's Brome
3460252043	29 Debs Alley	21.1241989	94.8659973	Bitter Fleabane
9036144918	2974 Ruskin Circle	59.9720688	73.1831589	Peltula Lichen
8172941703	82 Mcbride Drive	92.3736038	62.3000221	Ye Gu
3218561633	1 Cascade Way	23.7557087	52.9089737	Spearleaf
3967296473	953 Northview Lane	75.7861557	44.5052834	Hybrid Oak
3058754837	09280 Lake View Park	77.8859253	62.6309891	Salmonberry
5781975408	2 Maryland Road	62.0734749	30.7479134	Rockyplains Dwarf Morning-glory
8052255775	2 Caliangt Avenue	73.8411713	78.6513901	Antillean Furcraea
1245924074	95 Corben Circle	62.6522293	40.9188423	Jeweled Distaff Thistle
9310711057	8250 Nelson Plaza	95.7951431	34.6129608	San Diego Nightshade
\.


--
-- Data for Name: transaksi; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018046
--

COPY bike_sharing.transaksi (no_kartu_anggota, date_time, jenis, nominal) FROM stdin;
1	2018-11-06 04:10:49	duis	620888
2	2018-12-19 06:39:13	vel dapibus	593913.438
3	2019-04-01 03:05:17	dolor morbi	925269.438
4	2019-03-28 10:02:07	lectus	651710.25
5	2018-02-05 00:03:11	quis	855841.375
6	2018-11-13 10:41:50	tempus vel	212316.5
7	2018-03-13 08:30:18	in	880739.438
8	2018-09-14 13:05:25	accumsan	849146.562
9	2018-05-09 00:50:16	tincidunt eu	520672.562
10	2018-11-20 11:07:07	nulla mollis	845334
11	2018-02-03 19:12:39	a libero	309762.5
12	2018-04-03 03:09:48	at	851873.688
13	2019-01-01 14:05:03	pede	599428.688
14	2018-02-20 22:41:37	vestibulum aliquet	371392
15	2018-04-12 11:24:29	blandit	881412.938
16	2018-04-06 11:51:25	mi integer	603662.75
17	2018-07-09 00:48:30	aliquam lacus	545855.5
18	2018-06-14 21:56:41	nunc	545110.438
19	2018-08-16 09:53:10	aenean	967373.75
20	2018-09-05 07:52:35	dui	846601.688
21	2018-12-30 04:30:35	quis	601129.062
22	2018-11-12 16:46:35	odio	329219.812
23	2019-03-06 17:20:28	non	646266.438
24	2019-04-10 21:29:07	non	236683.859
25	2018-08-30 19:37:42	vitae	840992.75
26	2019-01-22 23:11:33	lacinia aenean	699783.125
27	2018-10-17 19:48:34	nec sem	897806.438
28	2018-11-13 11:28:52	rutrum rutrum	454801.188
29	2018-01-21 13:58:59	elementum in	885375.188
30	2018-06-03 05:45:37	tortor	340006.344
31	2018-04-08 07:06:35	lacus at	542795.812
32	2018-03-03 13:28:59	tristique	490313.906
33	2019-01-31 20:54:29	eget	625950.438
34	2019-01-31 02:37:48	nulla tellus	698712.562
35	2018-06-17 10:16:13	lectus in	987754.375
36	2018-02-03 15:00:45	ligula	824601.312
37	2018-10-08 02:17:04	curae	508040.938
38	2018-07-04 21:12:40	vitae	141995.484
39	2018-10-13 17:47:27	eu pede	211659.484
40	2018-07-22 21:47:56	ut volutpat	342551.844
41	2018-04-09 10:57:55	dui vel	449934.75
42	2018-08-12 16:58:26	sapien	114309.414
43	2019-01-23 11:24:28	orci luctus	972734.25
44	2018-02-27 21:06:11	in libero	115524.797
45	2018-10-19 03:46:40	sapien iaculis	547751.188
46	2018-02-24 11:55:37	morbi sem	283606.688
47	2018-12-08 13:23:40	est	314229.25
48	2018-02-12 15:31:11	sem	385738.312
49	2019-04-05 22:11:13	eleifend	470915.781
50	2018-01-17 13:47:02	nisi	390207.375
51	2019-03-25 01:39:26	nulla suscipit	284806.281
52	2019-02-06 06:50:14	nibh	384207.812
53	2018-11-16 23:05:13	aenean auctor	137441.703
54	2018-11-19 02:07:57	magnis	740358.5
55	2018-06-12 21:38:33	dolor	243939.359
56	2018-03-06 01:36:11	eu pede	205731.562
57	2019-02-27 22:12:31	erat id	484372
58	2018-12-12 05:11:16	mauris ullamcorper	642592.5
59	2018-07-14 20:55:37	ac enim	286661.438
60	2018-07-03 12:10:13	ornare	558059.375
61	2018-03-20 22:55:38	ligula	428709.25
62	2018-12-10 05:42:16	pede libero	123957.375
63	2018-07-13 12:15:14	semper	254993.688
64	2018-01-17 22:49:50	neque duis	532229.312
65	2018-08-31 21:14:12	amet turpis	609207
66	2018-07-06 10:51:24	ut	360434.438
67	2019-02-04 12:25:09	bibendum imperdiet	301670.625
68	2019-02-06 14:22:16	diam	723545.125
69	2018-04-24 22:02:20	imperdiet sapien	798454.125
70	2018-10-18 21:25:55	morbi	274415.281
71	2018-07-15 10:18:22	nullam	570135.438
72	2018-07-14 22:54:38	non	121603.336
73	2019-04-08 11:42:04	sapien	121506.188
74	2018-05-25 13:44:59	nisl duis	689228.25
75	2018-11-11 11:52:22	donec	444767.625
76	2018-08-31 15:09:16	eleifend luctus	330299.688
77	2018-01-21 07:58:22	ac	995763.875
78	2018-06-13 01:42:13	fusce	795030.75
79	2019-01-19 19:06:50	consequat metus	305397.906
80	2018-10-16 02:52:21	a suscipit	392951.188
81	2018-07-23 14:33:52	nisi at	302372.438
82	2018-08-25 10:20:38	habitasse	863776.938
83	2018-05-29 00:31:50	nunc purus	537503.875
84	2018-06-05 22:10:46	rutrum	990445.438
85	2018-12-09 16:00:23	massa	677168.25
86	2018-05-18 19:45:01	pretium	842765.938
87	2019-03-16 08:06:22	at	616000
88	2018-05-31 22:23:11	praesent	808467.562
89	2018-04-17 14:31:09	faucibus	499740.188
90	2018-10-31 12:38:29	enim in	844050.125
91	2018-12-17 10:01:05	et ultrices	526698.438
92	2018-03-03 14:53:41	lacus at	777839.875
93	2018-01-29 09:55:50	et ultrices	541963.812
94	2018-11-27 22:44:20	pellentesque quisque	600690.25
95	2018-01-21 07:26:22	dapibus	125487.859
96	2018-12-09 04:14:06	imperdiet et	471099.125
97	2018-12-20 06:49:34	magna	745955.375
98	2018-08-07 18:19:50	lacus	456907.375
99	2018-05-01 12:09:30	justo nec	686180.812
100	2018-03-16 07:25:14	pede	703587.312
\.


--
-- Data for Name: transaksi_khusus_peminjaman; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018046
--

COPY bike_sharing.transaksi_khusus_peminjaman (no_kartu_anggota, date_time, no_kartu_peminjam, datetime_pinjam, no_sepeda, id_stasiun) FROM stdin;
1	2018-11-06 04:10:49	1	2019-02-07 14:03:00	392	6312063456
2	2018-12-19 06:39:13	2	2019-01-04 03:39:00	801	1929769138
3	2019-04-01 03:05:17	3	2019-01-13 13:15:00	265	8636977565
4	2019-03-28 10:02:07	4	2019-01-04 21:37:00	389	2049674987
5	2018-02-05 00:03:11	5	2019-01-02 13:29:00	492	8151178731
6	2018-11-13 10:41:50	6	2019-02-20 12:18:00	836	9463608387
7	2018-03-13 08:30:18	7	2019-01-04 17:03:00	68	1278434561
8	2018-09-14 13:05:25	8	2019-01-21 20:31:00	7	1184421192
9	2018-05-09 00:50:16	9	2019-01-04 21:57:00	258	5495170035
10	2018-11-20 11:07:07	10	2019-01-02 13:20:00	208	1408981964
11	2018-02-03 19:12:39	11	2019-01-05 09:14:00	724	5112382019
12	2018-04-03 03:09:48	12	2019-01-20 18:46:00	745	8121095458
13	2019-01-01 14:05:03	13	2019-01-09 06:33:00	123	9733875382
14	2018-02-20 22:41:37	14	2019-01-11 00:59:00	452	2740942572
15	2018-04-12 11:24:29	15	2019-01-12 08:22:00	760	6356967539
16	2018-04-06 11:51:25	16	2019-01-03 15:35:00	147	3264485332
17	2018-07-09 00:48:30	17	2019-01-16 04:40:00	307	1472497806
18	2018-06-14 21:56:41	18	2019-01-05 18:27:00	477	3381904511
19	2018-08-16 09:53:10	19	2019-01-13 07:09:00	435	9894308870
20	2018-09-05 07:52:35	20	2019-02-16 03:54:00	947	8906292872
21	2018-12-30 04:30:35	21	2019-01-09 20:01:00	14	3011382212
22	2018-11-12 16:46:35	22	2019-02-04 07:32:00	885	1992476795
23	2019-03-06 17:20:28	23	2019-01-01 17:27:00	300	7058074250
24	2019-04-10 21:29:07	24	2019-01-03 05:33:00	717	7985455325
25	2018-08-30 19:37:42	25	2019-02-03 19:25:00	229	1521068163
26	2019-01-22 23:11:33	26	2019-01-10 09:47:00	71	2835053823
27	2018-10-17 19:48:34	27	2019-01-03 08:51:00	561	5485695175
28	2018-11-13 11:28:52	28	2019-01-16 18:39:00	425	5643252122
29	2018-01-21 13:58:59	29	2019-01-29 12:45:00	549	9666881568
30	2018-06-03 05:45:37	30	2019-01-29 02:50:00	97	4423323442
31	2018-04-08 07:06:35	31	2019-02-11 06:59:00	302	5590969344
32	2018-03-03 13:28:59	32	2019-02-08 15:20:00	780	6054624350
33	2019-01-31 20:54:29	33	2019-01-12 20:44:00	325	5659196726
34	2019-01-31 02:37:48	34	2019-01-29 07:05:00	112	3976597021
35	2018-06-17 10:16:13	35	2019-01-06 18:27:00	4250	2755842610
36	2018-02-03 15:00:45	36	2019-01-11 16:12:00	567	2037185186
37	2018-10-08 02:17:04	37	2019-01-30 16:01:00	428	8610262819
38	2018-07-04 21:12:40	38	2019-01-02 05:54:00	104	9603688799
39	2018-10-13 17:47:27	39	2019-02-10 17:16:00	69	3837112060
40	2018-07-22 21:47:56	40	2019-02-03 00:16:00	280	9873285052
41	2018-04-09 10:57:55	41	2019-01-09 10:49:00	228	1223890438
42	2018-08-12 16:58:26	42	2019-01-22 02:25:00	27	4379751506
43	2019-01-23 11:24:28	43	2019-01-26 06:47:00	442	4062913221
44	2018-02-27 21:06:11	44	2019-01-19 14:00:00	627	4752218682
45	2018-10-19 03:46:40	45	2019-01-19 01:34:00	60	1270208460
46	2018-02-24 11:55:37	46	2019-01-11 01:09:00	205	5442841814
47	2018-12-08 13:23:40	47	2019-02-01 07:18:00	534	8518426980
48	2018-02-12 15:31:11	48	2019-01-05 11:46:00	822	1775143765
49	2019-04-05 22:11:13	49	2019-01-17 10:49:00	701	2387465290
50	2018-01-17 13:47:02	50	2019-01-15 20:54:00	153	4428938386
51	2019-03-25 01:39:26	51	2019-06-19 20:54:00	850	9861679673
52	2019-02-06 06:50:14	52	2019-09-30 20:41:00	936	4327303813
53	2018-11-16 23:05:13	53	2019-05-16 14:17:00	973	7471830081
54	2018-11-19 02:07:57	54	2019-02-06 07:30:00	802	6302348535
55	2018-06-12 21:38:33	55	2019-12-19 12:36:00	253	1928294454
56	2018-03-06 01:36:11	56	2019-05-26 14:44:00	746	7920193925
57	2019-02-27 22:12:31	57	2019-02-17 23:15:00	533	8163912810
58	2018-12-12 05:11:16	58	2019-09-27 21:03:00	264	8064881764
59	2018-07-14 20:55:37	59	2019-09-29 23:15:00	960	5616283098
60	2018-07-03 12:10:13	60	2019-10-08 13:41:00	222	8564122545
61	2018-03-20 22:55:38	61	2019-01-02 15:29:00	131	7869118997
62	2018-12-10 05:42:16	62	2019-06-04 05:34:00	161	7417583551
63	2018-07-13 12:15:14	63	2019-12-30 16:22:00	515	9397281364
64	2018-01-17 22:49:50	64	2019-05-01 23:56:00	479	2995527281
65	2018-08-31 21:14:12	65	2019-05-04 00:46:00	223	7800955310
66	2018-07-06 10:51:24	66	2019-11-21 00:35:00	620	2728316655
67	2019-02-04 12:25:09	67	2019-03-22 10:49:00	710	4147615340
68	2019-02-06 14:22:16	68	2019-05-13 11:57:00	762	4755876679
69	2018-04-24 22:02:20	69	2019-11-22 02:02:00	113	6394905279
70	2018-10-18 21:25:55	70	2019-04-20 04:18:00	140	4813444075
71	2018-07-15 10:18:22	71	2019-01-21 23:17:00	305	4105629076
72	2018-07-14 22:54:38	72	2019-08-30 22:41:00	725	9262122055
73	2019-04-08 11:42:04	73	2019-04-01 12:10:00	882	4638515017
74	2018-05-25 13:44:59	74	2019-12-18 15:09:00	847	6794397584
75	2018-11-11 11:52:22	75	2019-07-23 01:58:00	648	6252197953
76	2018-08-31 15:09:16	76	2019-11-30 12:10:00	600	6626329256
77	2018-01-21 07:58:22	77	2019-02-18 02:05:00	346	9229997432
78	2018-06-13 01:42:13	78	2019-03-11 21:48:00	261	4072279458
79	2019-01-19 19:06:50	79	2019-06-22 16:26:00	834	7087202701
80	2018-10-16 02:52:21	80	2019-02-20 08:25:00	981	9660335346
81	2018-07-23 14:33:52	81	2019-04-03 13:42:00	277	6076291031
82	2018-08-25 10:20:38	82	2019-08-13 10:15:00	813	8322948117
83	2018-05-29 00:31:50	83	2019-09-10 20:11:00	828	1133043208
84	2018-06-05 22:10:46	84	2019-03-07 05:24:00	447	1356639989
85	2018-12-09 16:00:23	85	2019-02-14 07:13:00	935	9569758644
86	2018-05-18 19:45:01	86	2019-10-07 18:52:00	154	4454459520
87	2019-03-16 08:06:22	87	2019-04-20 19:40:00	985	7013257514
88	2018-05-31 22:23:11	88	2019-06-04 00:34:00	240	8241734299
89	2018-04-17 14:31:09	89	2019-01-14 00:49:00	853	9814019320
90	2018-10-31 12:38:29	90	2019-04-27 09:45:00	737	3038047517
91	2018-12-17 10:01:05	91	2019-05-30 21:18:00	372	3460252043
92	2018-03-03 14:53:41	92	2019-05-30 23:37:00	619	9036144918
93	2018-01-29 09:55:50	93	2019-01-23 11:46:00	218	8172941703
94	2018-11-27 22:44:20	94	2019-02-05 00:56:00	437	3218561633
95	2018-01-21 07:26:22	95	2019-02-02 10:34:00	275	3967296473
96	2018-12-09 04:14:06	96	2019-01-28 17:38:00	605	3058754837
97	2018-12-20 06:49:34	97	2019-10-02 04:44:00	638	5781975408
98	2018-08-07 18:19:50	98	2019-06-08 16:38:00	831	8052255775
99	2018-05-01 12:09:30	99	2019-04-24 11:00:00	658	1245924074
100	2018-03-16 07:25:14	100	2019-09-07 07:24:00	750	9310711057
\.


--
-- Data for Name: voucher; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018046
--

COPY bike_sharing.voucher (id_voucher, nama, kategori, nilai_poin, deskripsi, no_kartu_anggota) FROM stdin;
9971663792	Reine Coenraets	Health	4056	felis	1
9446622959	Mata Ringer	Outdoors	472	libero	2
9279893941	Auria Jouanet	Garden	6862	sed	3
4304961304	Kevyn McVaugh	Books	4785	vulputate	4
6422617612	Carlota Deeman	Outdoors	2935	orci	5
3748916087	Halli Baskett	Music	2096	eu	6
1936535236	Fey Mathes	Home	6556	nulla	7
4831293557	Otha Twigley	Automotive	3663	primis	8
6638625641	Lenore Gather	Games	1786	nonummy	9
2996261285	Maximilianus Ebden	Home	140	nullam	10
8036013138	Roi Schofield	Kids	4433	nam	11
3666422274	Bruce Fossick	Clothing	2434	nulla	12
2056619460	Sheridan Garlette	Games	654	nonummy	13
8424043876	Demetria Amys	Tools	2326	semper	14
5285450838	Garfield Richley	Grocery	1792	faucibus	15
9504896340	Lefty Hubbert	Electronics	9415	condimentum	16
7826622837	Zelda Valintine	Shoes	2789	pulvinar	17
9078137119	Eduard McCahey	Computers	5845	montes	18
3612652092	Suzann Dunsford	Health	7457	nec	19
9923898231	Adey Willars	Sports	414	habitasse	20
249764462	Vincenty Penson	Clothing	7444	orci	21
2779438091	George Celler	Electronics	4503	ridiculus	22
7743578983	Ayn Croster	Books	639	nulla	23
6894966877	Ddene Barthel	Electronics	6954	vitae	24
8142673206	Van Duffus	Shoes	6418	tincidunt	25
1304426176	Terri Tremberth	Grocery	4114	posuere	26
6719472127	Anneliese Grady	Automotive	763	vel	27
192379033	Tiler Beckenham	Computers	395	volutpat	28
2877046046	Erminia Dewbury	Outdoors	9382	vel	29
8590125056	Eugenie Damsell	Clothing	4161	est	30
7097840431	Kristine Smorthwaite	Electronics	125	pede	31
7709505753	Derril Frostdicke	Electronics	3371	semper	32
3898860130	Eleanore Dilley	Health	6954	suspendisse	33
7090937304	Andra Camilleri	Baby	4708	mauris	34
106148755	Agna Cartmel	Grocery	1923	parturient	35
1956975802	Hendrika Kingsnorth	Clothing	585	odio	36
6075888490	Piper Leggon	Beauty	6359	consequat	37
5314906440	Lissa Frusher	Electronics	7261	enim	38
7504075674	Miran Markos	Home	5286	indigo	39
1101600793	Flss Clague	Shoes	2661	luctus	40
449686631	Margareta Lindbergh	Shoes	2493	cum	41
1267651630	Port Cope	Home	3231	vestibulum	42
7393841499	Tabby MacRonald	Computers	2658	quam	43
3110086953	Nertie Bantham	Games	1928	mi	44
6270751967	Inness Camelli	Tools	7505	lectus	45
8800480249	Raquela Forseith	Beauty	6008	id	46
5241469217	Nikolaus Roll	Clothing	9814	justo	47
1035976038	Larine Grinston	Tools	8442	non	48
7308618945	Rozanna Battie	Garden	8701	sapien	49
123548472	Gery Blaycock	Electronics	8986	eu	50
\.


--
-- Name: acara acara_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.acara
    ADD CONSTRAINT acara_pkey PRIMARY KEY (id_acara);


--
-- Name: acara_stasiun acara_stasiun_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.acara_stasiun
    ADD CONSTRAINT acara_stasiun_pkey PRIMARY KEY (id_stasiun, id_acara);


--
-- Name: anggota anggota_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.anggota
    ADD CONSTRAINT anggota_pkey PRIMARY KEY (no_kartu);


--
-- Name: laporan laporan_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.laporan
    ADD CONSTRAINT laporan_pkey PRIMARY KEY (id_laporan, no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun);


--
-- Name: peminjaman peminjaman_datetime_pinjam_key; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_datetime_pinjam_key UNIQUE (datetime_pinjam);


--
-- Name: peminjaman peminjaman_id_stasiun_key; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_id_stasiun_key UNIQUE (id_stasiun);


--
-- Name: peminjaman peminjaman_no_kartu_anggota_key; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_no_kartu_anggota_key UNIQUE (no_kartu_anggota);


--
-- Name: peminjaman peminjaman_nomor_sepeda_key; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_nomor_sepeda_key UNIQUE (nomor_sepeda);


--
-- Name: peminjaman peminjaman_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_pkey PRIMARY KEY (no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun);


--
-- Name: penugasan penugasan_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.penugasan
    ADD CONSTRAINT penugasan_pkey PRIMARY KEY (ktp, start_datetime, id_stasiun);


--
-- Name: person person_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (ktp);


--
-- Name: petugas petugas_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.petugas
    ADD CONSTRAINT petugas_pkey PRIMARY KEY (ktp);


--
-- Name: sepeda sepeda_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.sepeda
    ADD CONSTRAINT sepeda_pkey PRIMARY KEY (nomor);


--
-- Name: stasiun stasiun_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.stasiun
    ADD CONSTRAINT stasiun_pkey PRIMARY KEY (id_stasiun);


--
-- Name: transaksi transaksi_date_time_key; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.transaksi
    ADD CONSTRAINT transaksi_date_time_key UNIQUE (date_time);


--
-- Name: transaksi_khusus_peminjaman transaksi_khusus_peminjaman_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.transaksi_khusus_peminjaman
    ADD CONSTRAINT transaksi_khusus_peminjaman_pkey PRIMARY KEY (no_kartu_anggota, date_time);


--
-- Name: transaksi transaksi_no_kartu_anggota_key; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.transaksi
    ADD CONSTRAINT transaksi_no_kartu_anggota_key UNIQUE (no_kartu_anggota);


--
-- Name: transaksi transaksi_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.transaksi
    ADD CONSTRAINT transaksi_pkey PRIMARY KEY (no_kartu_anggota, date_time);


--
-- Name: voucher voucher_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.voucher
    ADD CONSTRAINT voucher_pkey PRIMARY KEY (id_voucher);


--
-- Name: transaksi add_transaksi_pinjam; Type: TRIGGER; Schema: bike_sharing; Owner: db2018046
--

CREATE TRIGGER add_transaksi_pinjam AFTER INSERT ON bike_sharing.transaksi FOR EACH ROW EXECUTE PROCEDURE bike_sharing.add_transaksi_pinjam();


--
-- Name: peminjaman add_transaksi_pinjam; Type: TRIGGER; Schema: bike_sharing; Owner: db2018046
--

CREATE TRIGGER add_transaksi_pinjam AFTER INSERT ON bike_sharing.peminjaman FOR EACH ROW EXECUTE PROCEDURE bike_sharing.add_transaksi_pinjam();


--
-- Name: peminjaman biaya_pinjam; Type: TRIGGER; Schema: bike_sharing; Owner: db2018046
--

CREATE TRIGGER biaya_pinjam BEFORE INSERT ON bike_sharing.peminjaman FOR EACH ROW EXECUTE PROCEDURE bike_sharing.biaya_pinjam();


--
-- Name: peminjaman denda; Type: TRIGGER; Schema: bike_sharing; Owner: db2018046
--

CREATE TRIGGER denda AFTER UPDATE ON bike_sharing.peminjaman FOR EACH ROW EXECUTE PROCEDURE bike_sharing.denda();


--
-- Name: peminjaman laporan_24jam; Type: TRIGGER; Schema: bike_sharing; Owner: db2018046
--

CREATE TRIGGER laporan_24jam AFTER UPDATE ON bike_sharing.peminjaman FOR EACH ROW EXECUTE PROCEDURE bike_sharing.laporan_24jam();


--
-- Name: transaksi top_up; Type: TRIGGER; Schema: bike_sharing; Owner: db2018046
--

CREATE TRIGGER top_up AFTER INSERT ON bike_sharing.transaksi FOR EACH ROW EXECUTE PROCEDURE bike_sharing.top_up();


--
-- Name: voucher tukar_voucher; Type: TRIGGER; Schema: bike_sharing; Owner: db2018046
--

CREATE TRIGGER tukar_voucher AFTER INSERT ON bike_sharing.voucher FOR EACH ROW EXECUTE PROCEDURE bike_sharing.tukar_voucher();


--
-- Name: acara_stasiun acara_stasiun_id_acara_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.acara_stasiun
    ADD CONSTRAINT acara_stasiun_id_acara_fkey FOREIGN KEY (id_acara) REFERENCES bike_sharing.acara(id_acara) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: acara_stasiun acara_stasiun_id_stasiun_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.acara_stasiun
    ADD CONSTRAINT acara_stasiun_id_stasiun_fkey FOREIGN KEY (id_stasiun) REFERENCES bike_sharing.stasiun(id_stasiun) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: anggota anggota_ktp_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.anggota
    ADD CONSTRAINT anggota_ktp_fkey FOREIGN KEY (ktp) REFERENCES bike_sharing.person(ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sepeda fk_anggota; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.sepeda
    ADD CONSTRAINT fk_anggota FOREIGN KEY (no_kartu_penyumbang) REFERENCES bike_sharing.anggota(no_kartu) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: petugas fk_person; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.petugas
    ADD CONSTRAINT fk_person FOREIGN KEY (ktp) REFERENCES bike_sharing.person(ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: penugasan fk_petugas; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.penugasan
    ADD CONSTRAINT fk_petugas FOREIGN KEY (ktp) REFERENCES bike_sharing.petugas(ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sepeda fk_stasiun; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.sepeda
    ADD CONSTRAINT fk_stasiun FOREIGN KEY (id_stasiun) REFERENCES bike_sharing.stasiun(id_stasiun) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: penugasan fk_stasiun; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.penugasan
    ADD CONSTRAINT fk_stasiun FOREIGN KEY (id_stasiun) REFERENCES bike_sharing.stasiun(id_stasiun) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: laporan laporan_datetime_pinjam_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.laporan
    ADD CONSTRAINT laporan_datetime_pinjam_fkey FOREIGN KEY (datetime_pinjam) REFERENCES bike_sharing.peminjaman(datetime_pinjam) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: laporan laporan_id_stasiun_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.laporan
    ADD CONSTRAINT laporan_id_stasiun_fkey FOREIGN KEY (id_stasiun) REFERENCES bike_sharing.peminjaman(id_stasiun) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: laporan laporan_no_kartu_anggota_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.laporan
    ADD CONSTRAINT laporan_no_kartu_anggota_fkey FOREIGN KEY (no_kartu_anggota) REFERENCES bike_sharing.peminjaman(no_kartu_anggota) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: laporan laporan_nomor_sepeda_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.laporan
    ADD CONSTRAINT laporan_nomor_sepeda_fkey FOREIGN KEY (nomor_sepeda) REFERENCES bike_sharing.peminjaman(nomor_sepeda) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: peminjaman peminjaman_id_stasiun_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_id_stasiun_fkey FOREIGN KEY (id_stasiun) REFERENCES bike_sharing.stasiun(id_stasiun) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: peminjaman peminjaman_no_kartu_anggota_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_no_kartu_anggota_fkey FOREIGN KEY (no_kartu_anggota) REFERENCES bike_sharing.anggota(no_kartu) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: peminjaman peminjaman_nomor_sepeda_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_nomor_sepeda_fkey FOREIGN KEY (nomor_sepeda) REFERENCES bike_sharing.sepeda(nomor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_khusus_peminjaman transaksi_khusus_peminjaman_date_time_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.transaksi_khusus_peminjaman
    ADD CONSTRAINT transaksi_khusus_peminjaman_date_time_fkey FOREIGN KEY (date_time) REFERENCES bike_sharing.transaksi(date_time) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_khusus_peminjaman transaksi_khusus_peminjaman_datetime_pinjam_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.transaksi_khusus_peminjaman
    ADD CONSTRAINT transaksi_khusus_peminjaman_datetime_pinjam_fkey FOREIGN KEY (datetime_pinjam) REFERENCES bike_sharing.peminjaman(datetime_pinjam) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_khusus_peminjaman transaksi_khusus_peminjaman_id_stasiun_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.transaksi_khusus_peminjaman
    ADD CONSTRAINT transaksi_khusus_peminjaman_id_stasiun_fkey FOREIGN KEY (id_stasiun) REFERENCES bike_sharing.peminjaman(id_stasiun) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_khusus_peminjaman transaksi_khusus_peminjaman_no_kartu_anggota_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.transaksi_khusus_peminjaman
    ADD CONSTRAINT transaksi_khusus_peminjaman_no_kartu_anggota_fkey FOREIGN KEY (no_kartu_anggota) REFERENCES bike_sharing.transaksi(no_kartu_anggota) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_khusus_peminjaman transaksi_khusus_peminjaman_no_kartu_peminjam_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.transaksi_khusus_peminjaman
    ADD CONSTRAINT transaksi_khusus_peminjaman_no_kartu_peminjam_fkey FOREIGN KEY (no_kartu_peminjam) REFERENCES bike_sharing.peminjaman(no_kartu_anggota) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_khusus_peminjaman transaksi_khusus_peminjaman_no_sepeda_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.transaksi_khusus_peminjaman
    ADD CONSTRAINT transaksi_khusus_peminjaman_no_sepeda_fkey FOREIGN KEY (no_sepeda) REFERENCES bike_sharing.peminjaman(nomor_sepeda) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi transaksi_no_kartu_anggota_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.transaksi
    ADD CONSTRAINT transaksi_no_kartu_anggota_fkey FOREIGN KEY (no_kartu_anggota) REFERENCES bike_sharing.anggota(no_kartu) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: voucher voucher_no_kartu_anggota_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018046
--

ALTER TABLE ONLY bike_sharing.voucher
    ADD CONSTRAINT voucher_no_kartu_anggota_fkey FOREIGN KEY (no_kartu_anggota) REFERENCES bike_sharing.anggota(no_kartu) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

