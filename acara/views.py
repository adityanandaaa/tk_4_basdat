from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect,HttpResponse,JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
def acaracreate(request):
    # if(request.session['role']=='petugas'):
        response={}
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,bike_sharing')
        cursor.execute("select nama from stasiun")      
        hasil1 = cursor.fetchall()
        response['stasiun'] = hasil1
        return render(request, 'acara.html',response)
    # else:
    #     return HttpResponseRedirect('/')

def acaralist(request):
    response={}
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_station,bike_sharing')
    cursor.execute("select * from acara")
    
    hasil1 = cursor.fetchall()
    response['acara'] = hasil1
 
    return render(request, 'listacara.html',response)

def postacara(request):
    if(request.method=="POST"):
        judul = request.POST['judul']    
        deskripsi = request.POST['deskripsi']
        fee= request.POST['fee']
        mulai=request.POST['mulai']
        selesai=request.POST['selesai']
        list_stasiun=request.POST.getlist('stasiun')
        biaya = False
        if(fee == 'gratis'):
            biaya = True
        else:
            biaya = False
        
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,bike_sharing')
       
        
        for i in list_stasiun:
            cursor.execute("select id_stasiun from stasiun  where nama ='"+i+"' ")
            id_stasiun = cursor.fetchone()
           

        messages.error(request, "success")
        return HttpResponseRedirect('/acara/list')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/acara/create')

