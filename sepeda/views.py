from django.shortcuts import render
from django.db import connection

# Create your views here.
def sepedacreate(request):
    return render(request, 'sepeda.html')

def sepedalist(request):
    
    response={}
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_station,bike_sharing')
    cursor.execute("select * from sepeda;")

    hasil1 = cursor.fetchall()
    response['sepeda'] = hasil1
   

 
    return render(request, 'listsepeda.html',response)