from django.shortcuts import render
from django.db import connection

def tambahvoucher(request):
    return render(request, 'Penambahan.html')

def updatevoucher(request):
    return render(request, 'Update.html')

def pinjam(request):
    return render(request, 'Peminjaman.html')

def dafcer(request):
    response={}
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_station,bike_sharing')
    cursor.execute("select v.id_voucher,v.nama,v.kategori,v.nilai_poin,v.deskripsi,per.nama from voucher v,person per,anggota a where v.no_kartu_anggota =a.no_kartu and a.ktp = per.ktp;")

    hasil1 = cursor.fetchall()
    response['voucher'] = hasil1
   

 
    return render(request, 'Daftarvoucher.html',response)

def dafjem(request):
    response={}
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_station,bike_sharing')
    cursor.execute("select pem.no_kartu_anggota, sep.jenis,st.nama,pem.datetime_kembali, pem.biaya,pem.denda from peminjaman pem,sepeda sep,stasiun st,anggota a where pem.no_kartu_anggota = a.no_kartu and pem.nomor_sepeda = sep.nomor and pem.id_stasiun = st.id_stasiun;")

    hasil1 = cursor.fetchall()
    response['peminjaman'] = hasil1
   

 
    return render(request, 'Daftarpeminjaman.html',response)
# Create your views here.
