from django.contrib import admin
from django.urls import path,include
from . import views


app_name = 'voucher'
urlpatterns = [
    path('add',views.tambahvoucher, name ='tambahvoucher'),
    path('update',views.updatevoucher, name ='updatevoucher'),
    path('pinjam',views.pinjam, name ='pinjam'),
    path('dafcer',views.dafcer, name ='dafcer'),
    path('dafjem',views.dafjem, name ='dafjem'),

]